# to exit at first error, remove .ONESHELL:

SHELL := /bin/bash
SRC = $(wildcard notebooks/*.ipynb)

all: leximpact-common-python-libraries docs

package_version := $(shell grep "^version" pyproject.toml)

leximpact-common-python-libraries: $(SRC)
	-$(MAKE) lib
	touch leximpact-common-python-libraries

install:
	poetry install -E cache
	poetry run python -m ipykernel install --name leximpact-common-python-libraries-kernel --user
	poetry run pre-commit install


precommit:
	-poetry run pre-commit run --all-files
	-$(MAKE) lib
	-poetry run pre-commit run --all-files
	-echo "XXXXXXXX Last check XXXXXXXXXXX"
	-poetry run pre-commit run --all-files

lib:
	rm leximpact_common_python_libraries/*.py | true
	@echo $(package_version)
	sed -i "/^version/c\$(package_version)" settings.ini
	poetry run nbdev_export

docs_serve: docs
	cd docs && bundle exec jekyll serve

docs: $(SRC)
	poetry run nbdev_docs
	touch docs

test:
	poetry run pytest

test-nb:
	mkdir -p papermill
	poetry run papermill --execution-timeout 300 notebooks/cache.ipynb ./papermill/cache-paper.ipynb
	poetry run papermill --execution-timeout 300 notebooks/config.ipynb ./papermill/config-paper.ipynb
	poetry run papermill --execution-timeout 300 notebooks/logger.ipynb ./papermill/logger-paper.ipynb
	
pypi: dist
	poetry run twine upload --repository pypi dist/*

dist: clean
	poetry run python setup.py sdist bdist_wheel

clean:
	rm -rf dist
	rm -rf leximpact_common_python_libraries.egg-info
	rm -rf papermill

bump:
	poetry version patch
	$(MAKE) precommit
