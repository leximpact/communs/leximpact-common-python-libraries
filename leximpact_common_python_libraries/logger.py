# AUTOGENERATED! DO NOT EDIT! File to edit: ../notebooks/logger.ipynb.

# %% auto 0
__all__ = ["config", "logger", "formatter", "handler", "env_level"]

# %% ../notebooks/logger.ipynb 3
import logging

from .config import Configuration

config = Configuration()
logger = logging.getLogger("leximpact_socio-fisca-simu-etat")
# Clear logger to be sure to define it like we want
logger.handlers.clear()
formatter = logging.Formatter(
    "[%(name)s %(levelname)s @ %(asctime)s] %(message)s",
    datefmt="%H:%M:%S",
)
handler = logging.StreamHandler()
handler.setFormatter(formatter)


if not logger.handlers:
    logger.addHandler(handler)
env_level = config.get("LEXIMPACT_LOG_LEVEL", fail_on_missing=False)
if env_level is None:
    env_level = "DEBUG"
logger.setLevel(level=env_level)
