# Publish to conda

There is two way of publishing to conda:
- A fully automatic in CI that publish to an "openfisca" channel. See below for more information.
- A more complex for Conda-Forge.

We use both for openfisca-core but only _openfisca channel_ for this package.

## Automatic upload

The CI automaticaly upload the PyPi package, see the `.gitlab-ci.yml`, step `deploy_conda`.

## Manual actions made to make it works the first time

- Create an account on https://anaconda.org.
- Create a token on https://anaconda.org/leximpact/settings/access with _Allow write access to the API site_. Warning, it expire on 2023/01/13.
- Put the token in a CI env variable ANACONDA_TOKEN.

## Manual actions before CI exist

To create the package you can do the following in the project root folder:

- Edit `.conda/meta.yaml` and update it if needed:
    - Version number
    - Hash SHA256
    - Package URL on PyPi

- Build & Upload package:
    - If you want to use docker : `docker run -ti --rm=true -v $PWD:/data --workdir /data continuumio/anaconda3`
    - If you want to use podman : `podman run -ti --rm=true -v $PWD:/data --workdir /data docker.io/continuumio/anaconda3`
    - `conda install -c anaconda conda-build anaconda-client`
    - `conda config --add channels conda-forge`
    - `conda config --set channel_priority strict`
    - `conda build .conda`
    - `anaconda login`
    - `anaconda upload package-name-<VERSION>-py_0.tar.bz2`