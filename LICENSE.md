# Python librairies

By [Benoît Courty](mailto:benoit.courty@assemblee-nationale.fr)

Copyright (C) 2021, 2022, 2023 Assemblée nationale

https://git.leximpact.dev/leximpact/communs/leximpact-common-python-libraries/

> Python librairies LexImpact is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> Python librairies LexImpact is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
